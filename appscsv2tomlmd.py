#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv
import urllib.request
import os
import sys
import io
import utils


def read_csv(path, column_names):
    with open(path, newline='') as f:
        # why newline='': see footnote at the end of https://docs.python.org/3/library/csv.html
        reader = csv.reader(f)
        for row in reader:
            if reader.line_num == 1:
                continue # skips the first row
            record = {name: value for name, value in zip(column_names, row)}
            yield record


# This one is bad and needs to be extended so that developers of Plasma and GNOME core apps get properly credited
def get_app_author(str):
    urlparts = []
    urlparts = str.split("/")
    if urlparts[2] == "code.qt.io":
        app_author = "Qt Company"
    elif urlparts[-2] == "plasma-mobile":
        app_author ="Plasma Mobile Developers"
    elif str == "https://gitlab.com/postmarketOS/postmarketos-tweaks":
        app_author = "martijnbraam"
    elif urlparts[-2] == "postmarketOS":
        app_author = "postmarketOS Developers"
    elif urlparts[-1] == "giara":
        app_author = "gabmus"
    elif urlparts[-2] == "GNOME" or urlparts[-2] == "World":
        app_author = "GNOME Developers"
    else:
        app_author = urlparts[-2].lower().replace("~", "")
    return app_author

# Taxonomy for distro users
def get_packaged(aur,pmos,deb,flathub):
    packages = []
    if aur != "":
        packages.append("AUR")
    if pmos != "":
        packages.append("postmarketOS")
    if deb != "":
        packages.append("Debian")
    if utils.is_flathub(flathub):
        packages.append("Flathub")
    packaged = ' '.join(packages)
    return packaged

# only appscsv2tomlmd.py
def write_markdown(doc,filename):
    with open('./output/'+filename, 'w') as file:
        file.write(doc)
    return file
    
# only appscsv2tomlmd
def generate_individual_markdown_files(records): # Create individual files with Zola TOML frontmatter as specified in https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/16
# use TOML module for this  https://pypi.org/project/toml/   ? 
    column_names = utils.get_column_names(records)
    #generate a filename, I suppose this should be a seperate function
    for record in records:
        lines = []
        if record["app_id"] != "":
            record["filename"] = record["app_id"].lower() + ".md"
            filename = record["filename"]
        else:
            urllist = []
            url = record["repository"]
            urllist = url.split("/")
            urllist = [i for i in urllist if i]
            if urllist[2] != "code.qt.io":
                record["filename"] = "noappid." + urllist[-2].lower().replace("~", "") +"."+ urllist[-1].lower().replace("_","-") + ".md"
                filename = record["filename"]
            else:
                record["filename"] = "noappid.qt." + record["name"].lower().replace(" ","-") + ".md"
                filename = record["filename"]
        print(filename)
        # Generating the contents of the actual markdown file
        lines.append('+++\n')
        lines.append('title = "'+ record["name"] +'"\n')
        lines.append('description = "' + record["summary"].replace("\"","") + '"\n')
        lines.append('date = "'+ record["date"] +'"\n')
        if record["updated"] != "":
            lines.append('updated = "'+ record["updated"] +'"\n')
        #taxonomies start here
        lines.append('[taxonomies]\nmobile_compatibility = ["'+ record["mobile_compatibility"]+'"]\n')
        lines.append('frameworks = ['+ utils.get_toml_array_value(record["framework"],",") +']\n')
        lines.append('categories = ['+ utils.get_toml_array_value(record["category"],",") +']\n')
        lines.append('freedesktop_categories = ['+ utils.get_toml_array_value(record["freedesktop_categories"],",") +']\n')
        lines.append('project_licenses = ['+ utils.get_toml_array_value(record["license"],"AND") +']\n')
        if record["metadata_license"] != "":
            lines.append('metadata_licenses = ['+ utils.get_toml_array_value(record["metadata_license"],"AND") +']\n')
        lines.append('build_systems = ['+ utils.get_toml_array_value(record["buildsystem"],",") +']\n')
        lines.append('programming_languages = ['+ utils.get_toml_array_value(record["languages"],",") +']\n')
        if record["status"] != "":
            lines.append('status = ['+ utils.get_toml_array_value(record["status"],",") +']\n')
        if record["service"] != "":
            lines.append('services = ['+ utils.get_toml_array_value(record["service"],",") +']\n')
        if record["backend"] != "":
            lines.append('backends = ['+ utils.get_toml_array_value(record["backend"],",") +']\n')
        lines.append('app_author = ["'+ get_app_author(record["repository"])+'",]\n')
        if record["postmarketos"] != "" or record["aur"] != "" or record ["debian"] != "" or utils.is_flathub(record["flatpak"]):
            lines.append('packaged_in = [' + utils.get_toml_array_value(get_packaged(record["aur"],record["postmarketos"],record["debian"],record["flatpak"])," ")+']\n')
        # extra values start here
        lines.append('[extra]\nrepository = "'+ record["repository"]+'"\n')
        optional_single_values = ["app_id","scale_to_fit","homepage","donations","translations","appstream_xml_url","updated_by",]
        for value in optional_single_values:
            if record[value] != "":
                lines.append(value+' = "'+ record[value] +'"\n')
        lines.append('author = "'+ record["reporter"] +'"\n')
        if record["summary_source"] != "" or "no quotation":
            lines.append('summary_source_url = "'+record["summary_source"]+'"\n')
        if record["repology"] != "":
            lines.append('repology = ['+ utils.get_toml_array_value(record["repology"]," ")+']\n')
        if record["flatpak"] != "":
            if utils.is_flathub(record["flatpak"]):
                lines.append('flathub = "'+ record["flatpak"]+'"\n')
            else:
                lines.append('flatpak_link = "'+ record["flatpak"]+'"\n')
        if record["bugtracker"] == "":
            if utils.is_github_or_gitea(record["repository"]):
                lines.append('bugtracker = "' + record["repository"]+'/issues/"\n')
            elif utils.is_gitlab(record["repository"]):
                lines.append('bugtracker = "' + record["repository"]+'/-/issues/"\n')
        elif utils.is_link(record["bugtracker"]):
            lines.append('bugtracker = "' + record["bugtracker"]+'"\n')
        if record["more_information"] != "":
            lines.append('more_information = ['+ utils.get_toml_array_value(record["more_information"]," ")+']\n')
        if record["screenshots"] != "":
            shotstring = record["screenshots"] 
            shotlist = []
            shotlist = shotstring.split(" ")
            imglpa = [shot for shot in shotlist if "img.linuxphoneapps.org" in shot]
            imglpa = set(imglpa)
            shotlist = set(shotlist)
            shotlink = shotlist.difference(imglpa)
            if len(imglpa) > 0:
                imglpastr = ' '.join(sorted(imglpa))
                lines.append('screenshots_img = ['+ utils.get_toml_array_value(imglpastr," ")+']\n')
            if len(shotlink) > 0:
                shoturlstr = ' '.join(shotlink)
                lines.append('screenshots = ['+ utils.get_toml_array_value(shoturlstr," ")+']\n')
        lines.append('+++\n\n')
        if record["description"] != "":
            lines.append('### Description\n\n'+record["description"]+' ')
        if record["description_source"] != "" and "no quotation":
            lines.append('[Source]('+record["description_source"]+')\n\n')
        else:
            lines.append('\n\n')
        if record["notice"] != "":
            lines.append('### Notice\n\n'+record["notice"])
        page = ''.join(lines)
        file = write_markdown(page,filename)



records = list(read_csv('apps.csv', 'name repository homepage bugtracker donations translations more_information license metadata_license category summary summary_source description description_source screenshots mobile_compatibility status notice framework backend service app_id scale_to_fit flatpak repology aur postmarketos debian appstream_link appstream_xml_url freedesktop_categories languages buildsystem date reporter updated updated_by'.split()))

# generate per app markdown files
generate_individual_markdown_files(records)
