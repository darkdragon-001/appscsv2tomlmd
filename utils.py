#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime

def get_current_date():
    time = datetime.datetime.now()
    return time.strftime("%G-%m-%d")
    
def is_link(str):
    if str.startswith( 'http' , 0 , 4 ):
        return True

# to utils
def sort_by_name(e):
    return e['name']

#to utils
def is_flathub(str):
    if str.startswith( 'https://flathub.org' , 0 , 19 ):
        return True

def is_github_or_gitea(str):
    if str.startswith('https://github.com/', 0, 19) or str.startswith('https://codeberg.org/', 0, 21) or str.startswith('https://code.smolnet.org/', 0, 25):
        return True
    
def is_gitlab(str): 
    if str.startswith('https://gitlab.com/', 0, 19) or str.startswith('https://gitlab.gnome.org/', 0, 25) or str.startswith('https://invent.kde.org/', 0, 23) or str.startswith('https://framagit.org/', 0, 21) or str.startswith('https://dev.gajim.org/', 0, 22) or str.startswith('https://source.puri.sm/',0, 23) or str.startswith('https://gitlab.shinice.net/', 0, 27) or str.startswith('https://git.eyecreate.org', 0, 25):
        return True

def get_toml_array_value(str,divider):
    list = []
    result = []
    list = str.split(divider)
    for i in list:
        result.append('"'+ i.strip() +'",')
    toml_array = ''.join(result)
    return toml_array

# to utils
def get_column_names(records):
    # records is expected to be a list of dicts
    columns = []
    # first detect all posible keys (field names) that are present in records
    for record in records:
        for name in record.keys():
            if name not in columns:
                columns.append(name)
    return columns